// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.Systems;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.Swerve.SwerveSubsystem;

public class SelfStraight extends Command {
  SwerveSubsystem swerve;
  double forwardTranslation;
  double sideTranslation;
  Joystick joystick;
  public SelfStraight(SwerveSubsystem swerve, Joystick joystick) {
    this.joystick = joystick;
    this.swerve = swerve;
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {}

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    forwardTranslation = joystick.getRawAxis(1);
    sideTranslation = joystick.getRawAxis(0);
    swerve.driveHeading(
      forwardTranslation,
      sideTranslation,
      (Math.abs(swerve.getYaw().getRadians()) > Math.PI / 2) ? Math.PI : 0,
      true
    );
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    if(Math.abs(joystick.getRawAxis(2)) > 0.05) {
      return true;
    }
    return false;
  }
}
