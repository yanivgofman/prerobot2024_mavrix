package frc.robot;

import java.util.ArrayList;
import java.util.List;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.pathplanner.lib.util.PIDConstants;

import edu.wpi.first.apriltag.AprilTag;
import edu.wpi.first.math.MatBuilder;
import edu.wpi.first.math.Matrix;
import edu.wpi.first.math.Nat;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Rotation3d;
import edu.wpi.first.math.geometry.Transform3d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.geometry.Translation3d;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.numbers.N1;
import edu.wpi.first.math.numbers.N3;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.units.Unit;
import edu.wpi.first.wpilibj.PowerDistribution;
import frc.robot.subsystems.Swerve.COTSFalconSwerveConstants;


public final class Constants {

  public static double LoopTime = 0.02;

  public static double RobotVoltage = 12.0;

  public static boolean Telemetry = true;

  public static boolean logging = true;
  
  public static class Swerve {

    public static double SpeakerRedSideY = 2.5;
    public static double SpeakerBlueSideY = 5;

    
    //width with bumpers
    public static double kRobotWidthWithBumpers = Units.inchesToMeters(Units.metersToInches(0.75));

    //width witdout bumpers
    public static double kTrackWidth = 0.75;

    //steer wheels radius
    public static double kWheelRadius = Units.inchesToMeters(2);

    //Swerve Module MK4_L2 gear ratios caculated
    public static double kDriveGearRatio = (50.0 / 14.0) * (17.0 / 27.0) * (45.0 / 15.0);
    public static double kSteerGearRatio = 150.0 / 7.0;

    //FeedForward variables
    /* Drive Motor Characterization Values
     * Divide SYSID values by 12 to convert from volts to percent output for CTRE */
    public static final double DRIVE_KS = 0.32 / 12.0; // 0.65559
    public static final double DRIVE_KV = 1.51 / 12.0; // 1.93074
    public static final double DRIVE_KA = 0.27 / 12.0; // 0.00214

    //robot starting angle
    public static Rotation2d kStartingHeading = new Rotation2d();

    
    //max speed calcuated 
    public static double kMaxSpeed = (FalconConstants.MAX_RPM / 100.0) * kWheelRadius * 2 * Math.PI / kDriveGearRatio;

    //max angular speed
    public static double kMaxAngularSpeed = kMaxSpeed / ((kTrackWidth / 2) * Math.sqrt(2));

    //max angular acceleration
    public static double kMaxAngularAccel = 3 * 2 * Math.PI; // 8 rotations per second per second


    //swerve Falcon Constants
    public static final COTSFalconSwerveConstants kModuleConstants = COTSFalconSwerveConstants.SDSMK4i(COTSFalconSwerveConstants.DriveGearRatios.SDSMK4i_L2);

    /* Swerve Current Limiting */
    public static final int kAngleContinuousCurrentLimit = 25;
    public static final int kAnglePeakCurrentLimit = 40;
    public static final double kAnglePeakCurrentDuration = 0.1;
    public static final boolean kAngleEnableCurrentLimit = true;
    public static final int kDriveContinuousCurrentLimit = 35;
    public static final int kDrivePeakCurrentLimit = 60;
    public static final double kDrivePeakCurrentDuration = 0.1;
    public static final boolean kDriveEnableCurrentLimit = true;
    private PowerDistribution pdh;


    /* Motor inversions */
    public static final boolean kDriveMotorInvert = true;//kModuleConstants.driveMotorInvert;
    public static final boolean kAngleMotorInvert = kModuleConstants.angleMotorInvert;

    /* Neutral Modes */
    public static final NeutralMode kDriveNeutralMode = NeutralMode.Brake;
    public static final NeutralMode kAngleNeutralMode = NeutralMode.Coast;

    /* Drive Motor PID Values */
    public static final double kDriveP = 0.05;
    public static final double kDriveI = 0.0;
    public static final double kDriveD = 0.0;
    public static final double kDriveF = 0.0;

    /* Ramp values for drive motors in open and closed loop driving. */
    // Open loop prevents throttle from changing too quickly.
    // It will limit it to time given (in seconds) to go from zero to full throttle.
    // A small open loop ramp (0.25) helps with tread wear, tipping, etc
    public static final double kOpenLoopRamp = 0.25;
    public static final double kClosedLoopRamp = 0.0;

    public static final double kWheelCircumference = kModuleConstants.wheelCircumference;

    /* Motor gear ratios */
    public static final double kAngleGearRatio = kModuleConstants.angleGearRatio;

    public static final boolean kInvertGyro = false; // Make sure gyro is CCW+ CW- // FIXME: Swerve

    public static final double kSlowDriveFactor = 0.2;
    public static final double kSlowRotFactor = 0.1;

    public static final SwerveDriveKinematics Kinematics = new SwerveDriveKinematics(
            new Translation2d(kTrackWidth / 2, kTrackWidth / 2),
            new Translation2d(kTrackWidth / 2, -kTrackWidth / 2),
            new Translation2d(-kTrackWidth / 2, kTrackWidth / 2),
            new Translation2d(-kTrackWidth / 2, -kTrackWidth / 2)
    );
    //translational PID
    public static double kTranslationalP = 3.2;
    public static double kTranslationalD = 0;//0.001

    //The PIDs for PathPlanner Command
    public static double kPathplannerHeadingP = 3.5;
    public static double kPathplannerHeadingD = 0;

    public static double kPathplannerTranslationalP = 6;
    public static double kPathplannerTranslationalD = 0;

     // heading PID
     //4.6
     public static double kHeadingP =5;
     public static double kHeadingD = 1.2;

     public static double kRingP = 0.12;
    //  public static double kStockerP =4.8;


    //swerve components id
    public static class Id{
      public static final int kPigeonId = 40;

      public static final int kDriveFrontLeft = 43;
      public static final int kSteerFrontLeft = 42;
      public static final int kEncoderFrontLeft = 41;
      public static final double kSteerOffsetFrontLeft = Math.toRadians(23.203);//-3.060285486280918+Math.PI;
      // public static final double kSteerOffsetFrontLeft = Math.toRadians(201);//-3.060285486280918+Math.PI;

  
      public static final int kDriveFrontRight = 33;
      public static final int kSteerFrontRight = 32;
      public static final int kEncoderFrontRight = 31;
      public static final double kSteerOffsetFrontRight = Math.toRadians(246.182);//159.285;
      // public static final double kSteerOffsetFrontRight = Math.toRadians(155);//159.285;

  
      public static final int kDriveBackLeft = 13;
      public static final int kSteerBackLeft = 12;
      public static final int kEncoderBackLeft = 11;
      public static final double kSteerOffsetBackLeft = Math.toRadians(174.990);//0.650406539440155+Math.PI;
      // public static final double kSteerOffsetBackLeft = Math.toRadians(124);//159.285;

  
      public static final int kDriveBackRight = 23;
      public static final int kSteerBackRight = 22;
      public static final int kEncoderBackRight = 21;
      public static final double kSteerOffsetBackRight = Math.toRadians(211.729);//2.771897681057453;
      // public static final double kSteerOffsetBackRight = Math.toRadians(156);//2.771897681057453;

    }
   
  }

  
public static class VisionConstants {

  // Increasing this makes pose estimation trust vision measurements less as distance from Apriltags increases
  // This is how much is added to std dev for vision when closest visible Apriltag is 1 meter away
  public static final double kVisionPoseStdDevFactor = 1.0;

  // TODO: check/tune vision weight
  // How much to trust vision measurements normally

  // TODO: check/tune vision weight
  // How much to trust vision measurements normally
  public static final Matrix<N3, N1> kBaseVisionPoseStdDevs = new MatBuilder<>(Nat.N3(), Nat.N1()).fill(
    0, // x in meters (default=0.9)
    0, // y i meters (default=0.9)
    1000  // heading in radians. The gyroscope is very accurate, so as long as it is reset correctly it is unnecessary to correct it with vision
  );

  public static final Matrix<N3, N1> kBaseVPoseStdDevs = new MatBuilder<>(Nat.N3(), Nat.N1()).fill(
    0.9, // x in meters (default=0.9)
    0.9, // y in meters (default=0.9)
    1000  // heading in radians. The gyroscope is very accurate, so as long as it is reset correctly it is unnecessary to correct it with vision
  );
  // How much to trust vision after passing over the charge station
  // Should be lower to correct pose after charge station
  public static final Matrix<N3, N1> kChargeStationVisionPoseStdDevs = new MatBuilder<>(Nat.N3(), Nat.N1()).fill(
    0.01, // x in meters
    0.01, // y in meters
    1000   // heading in radians. The gyroscope is very accurate, so as long as it is reset correctly it is unnecessary to correct it with vision
  );

  // If vision is enabled
  public static final boolean kEnabled = true;

  // The angle to use charge station vision at in degrees. 
  // If the pitch or the roll of the robot is above this amount, it will trust vision more for a bit.
  public static final double kChargeStationAngle = 2.5;

  //TODO: Change these to whatever the actual distances are
  /**  How far from the grid the robot should be to score for alignment, in meters */
  public  double kGridDistanceAlignment = 0 + Swerve.kRobotWidthWithBumpers / 2; // meters
  
  /** How far from the shelf the robot should be to intake for alignment, in meters */
  public  double kShelfDistanceAlignment = 0.5 + Swerve.kRobotWidthWithBumpers / 2;

  public static final Transform3d APRILTAG_CAMERA_TO_ROBOT = new Transform3d(
            //0.3875
            new Translation3d(0,Units.inchesToMeters(20),0),
            new Rotation3d(Units.degreesToRadians(120), 0,0.0));

  public static final Transform3d CAMERA_TO_ROBOT = new Transform3d(
            //0.3875
            new Translation3d(0,Units.inchesToMeters(0),Units.inchesToMeters(20)),
            new Rotation3d(Units.degreesToRadians(120), 0,0.0));

}
    
    public static class AutoConstants{    
    public static final double wheelRotationPerSecond = 3; // m/s
    public static final double MAX_AUTO_ACCEL = 2; // m/s^%2

    // public static PIDConstants translationController = new PIDConstants( 8.5, 0, 0.025);
    public static PIDConstants translationController = new PIDConstants(5, 0, 0);
    public static PIDConstants rotationController = new PIDConstants(5.5,0, 0);

  }

  public class FalconConstants{
    
    public static final int FIRMWARE_VERSION = 5633; // version 22.1.1.0
    public static final boolean BREAK_ON_WRONG_FIRMWARE = false; // TODO: fix issue that make the robot break

    public static final double RESOLUTION = 2048;
    public static final double MAX_RPM = 6380.0; // Rotations per minute

    // These are the default values

    // Stator
    public static final boolean STATOR_LIMIT_ENABLE = false; // enabled?
    public static final double STATOR_CURRENT_LIMIT = 100; // Limit(amp
    public static final double STATOR_TRIGGER_THRESHOLD = 100; // Trigger Threshold(amp)
    public static final double STATOR_TRIGGER_DURATION = 0; // Trigger Threshold Time(s)

    // Supply
    public static final boolean SUPPLY_LIMIT_ENABLE = false; // enabled?
    public static final double SUPPLY_CURRENT_LIMIT = 40; // Limit(amp), current to hold after trigger hit
    public static final double SUPPLY_TRIGGER_THRESHOLD = 55; // (amp), amps to activate trigger
    public static final double SUPPLY_TRIGGER_DURATION = 3; // (s), how long after trigger before reducing

  }

  
  public static class FieldConstants {
    public static final double FIELD_LENGTH = Units.inchesToMeters(54 * 12 + 3.25); // meters
    public static final double FIELD_WIDTH = Units.inchesToMeters(26 * 12 + 3.5); // meters

    // Array to use if it can't find the April tag field layout
    public static final ArrayList<AprilTag> APRIL_TAGS = new ArrayList<AprilTag>(List.of(
            new AprilTag(1, new Pose3d(Units.inchesToMeters(15.08), Units.inchesToMeters(0.25), Units.inchesToMeters(1.36), new Rotation3d(0.0, 0.0, (Math.PI /3 )* 2))),
            new AprilTag(2, new Pose3d(Units.inchesToMeters(16.19 ), Units.inchesToMeters(0.88), Units.inchesToMeters(1.36), new Rotation3d(0.0, 0.0, (Math.PI /3 )* 2))),
            new AprilTag(3, new Pose3d(Units.inchesToMeters(16.58 ), Units.inchesToMeters(4.98), Units.inchesToMeters(1.45), new Rotation3d(0.0, 0.0, Math.PI))),
            new AprilTag(4, new Pose3d(Units.inchesToMeters(16.58 ), Units.inchesToMeters(5.55), Units.inchesToMeters(1.45), new Rotation3d(0.0, 0.0, Math.PI))),
            new AprilTag(5, new Pose3d(Units.inchesToMeters(14.70 ), Units.inchesToMeters(8.2), Units.inchesToMeters(1.36), new Rotation3d(0.0, 0.0, -Math.PI/2))),
            new AprilTag(6, new Pose3d(Units.inchesToMeters(1.84 ), Units.inchesToMeters(8.2), Units.inchesToMeters(1.36), new Rotation3d(0.0, 0.0, -Math.PI/2))),
            new AprilTag(7, new Pose3d(Units.inchesToMeters(-0.04), Units.inchesToMeters(5.55), Units.inchesToMeters(1.45), new Rotation3d(0.0, 0.0, 0.0))),
            new AprilTag(8, new Pose3d(Units.inchesToMeters(-0.04), Units.inchesToMeters(4.98), Units.inchesToMeters(1.45), new Rotation3d(0.0, 0.0, 0.0))),
            new AprilTag(9, new Pose3d(Units.inchesToMeters(0.36 ), Units.inchesToMeters(0.88), Units.inchesToMeters(1.36), new Rotation3d(0.0, 0.0, Math.PI/3))),                                                                   
            new AprilTag(10, new Pose3d(Units.inchesToMeters(1.46 ), Units.inchesToMeters(0.25), Units.inchesToMeters(1.36), new Rotation3d(0.0, 0.0, Math.PI/3))),                                                                   
            new AprilTag(11, new Pose3d(Units.inchesToMeters(11.90 ), Units.inchesToMeters(3.71), Units.inchesToMeters(1.32), new Rotation3d(0.0, 0.0, -Math.PI/3))),                                                                   
            new AprilTag(12, new Pose3d(Units.inchesToMeters(11.90 ), Units.inchesToMeters(4.50), Units.inchesToMeters(1.32), new Rotation3d(0.0, 0.0, Math.PI/3))),                                                                   
            new AprilTag(13, new Pose3d(Units.inchesToMeters(11.20 	), Units.inchesToMeters(4.11), Units.inchesToMeters(1.32), new Rotation3d(0.0, 0.0,Math.PI))),                                                                   
            new AprilTag(14, new Pose3d(Units.inchesToMeters(5.32), Units.inchesToMeters(4.11), Units.inchesToMeters(1.32), new Rotation3d(0.0, 0.0, 0.0))),                                                                   
            new AprilTag(15, new Pose3d(Units.inchesToMeters(4.64), Units.inchesToMeters(4.50), Units.inchesToMeters(1.32), new Rotation3d(0.0, 0.0, Math.PI / 2))),                                                                   
            new AprilTag(16, new Pose3d(Units.inchesToMeters(4.64), Units.inchesToMeters(3.71), Units.inchesToMeters(1.32), new Rotation3d(0.0, 0.0, -Math.PI / 2)))));
  }

  public class OIConstants {
  
      public static final int DRIVER_JOY = 0;
  
      public static final int OPERATOR_JOY = 1;
      public static final int TEST_JOY = 2;
      public static final int MANUAL_JOY = 3;
      public static final double DEADBAND = 0.005;
  
      //TODO: change sensitivity to 1?
  
      public static final double TRANSLATIONAL_SENSITIVITY = 1;
      public static final double TRANSLATIONAL_EXPO = 2;
      public static final double TRANSLATIONAL_DEADBAND = 0.05;
      public static final double TRANSLATIONAL_SLEWRATE = 20;
      public static final boolean FIELD_RELATIVE = true;
      public static final double ROTATION_SENSITIVITY = 1;
  
      public static final double ROTATION_EXPO = 4;
      public static final double ROTATION_DEADBAND = 0.01;
      public static final double ROTATION_SLEWRATE = 10;
      public static final double HEADING_SENSITIVITY = 4;
  
      public static final double HEADING_EXPO = 2;
      public static final double HEADING_DEADBAND = 0.05;
      public static final boolean CONSTANT_HEADING_MAGNITUDE = false;
      public static final boolean INVERT = false;

  }
}

