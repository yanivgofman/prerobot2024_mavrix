package frc.robot.commands;

import java.util.ArrayList;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Systems.StockerOdometry;
import frc.robot.subsystems.Swerve.SwerveSubsystem;
import frc.util.PS5ControllerDriverConfig;
import frc.util.Vision;


/**
 * Default drive command. Drives robot using driver controls.
 */
public class SwerveDefaultCommand extends CommandBase {
    private final SwerveSubsystem swerve;
    private final PS5ControllerDriverConfig Driver;
    private final ShuffleboardTab drivetrainTab;
    private ArrayList<Boolean> presses;
    private double targetStockerAngle = 0;

    private boolean isPOV = false;

    public SwerveDefaultCommand(
            SwerveSubsystem swerve,
            PS5ControllerDriverConfig Driver,
            ShuffleboardTab drivetrainTab) {
                
        this.drivetrainTab = drivetrainTab;
        this.swerve = swerve;
        this.Driver = Driver;
        addRequirements(swerve);
    }

    @Override
    public void initialize() {
        swerve.enableStateDeadband(true);
    }

    @Override
    public void execute() {
        double forwardTranslation =Driver.getForwardTranslation();
        double sideTranslation = Driver.getSideTranslation();
        
        double rotation = Driver.getRotation();

        double slowFactor = Driver.getIsSlowMode() ? Constants.Swerve.kSlowDriveFactor : 1;

        forwardTranslation *= slowFactor;
        sideTranslation *= slowFactor;
        rotation *= Driver.getIsSlowMode() ? Constants.Swerve.kSlowRotFactor : 1;

        int allianceReversal = DriverStation.getAlliance().get() == Alliance.Blue? 1 : -1;
        forwardTranslation *= allianceReversal;
        sideTranslation *= allianceReversal;
        // System.out.println(swerve.getLatesetCam().getBestTarget().getYaw());

        if (Driver.getIsAlign()) {
            swerve.driveHeading(
                    forwardTranslation,
                    sideTranslation,
                    (Math.abs(swerve.getYaw().getRadians()) > Math.PI / 2) ? Math.PI : 0,
                    true
                               );
        }else{
        swerve.drive(
                forwardTranslation,
                sideTranslation,
                rotation,
                true,
                false
        );
    }


        if (Driver.getResetOdometry()) {
            swerve.resetOdometry( new Pose2d(  new Translation2d( 1.5 , Constants.Swerve.SpeakerRedSideY), new Rotation2d()));
        }

        // if( swerve.getStockerAngle() > 0)
        //     targetStockerAngle = -(90 + swerve.getStockerAngle());
        // else if( swerve.getStockerAngle() < 0)
        //     targetStockerAngle = 90 + Math.abs(-swerve.getStockerAngle());

    }

}
  