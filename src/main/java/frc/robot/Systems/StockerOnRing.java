package frc.robot.Systems;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.Swerve.SwerveSubsystem;

public class StockerOnRing extends Command {
  SwerveSubsystem swerve;
  Joystick joystick;
  double forwardTranslation;
  double sideTranslation;
  public StockerOnRing(SwerveSubsystem swerve, Joystick joystick) {
    this.swerve = swerve;
    this.joystick = joystick;
    addRequirements(swerve);
  }

  @Override
  public void initialize() {}

  @Override
  public void execute() {
    forwardTranslation = joystick.getRawAxis(1);
    sideTranslation = joystick.getRawAxis(0);

    swerve.driveToRing(
      forwardTranslation,
      sideTranslation,
      0,
      true
    );

  }

  @Override
  public void end(boolean interrupted) {

  }

  @Override
  public boolean isFinished() {
    if(Math.abs(joystick.getRawAxis(2)) > 0.05 || !swerve.hasCamTarget()) {
      return true;
    }
    return false;
  }
}
