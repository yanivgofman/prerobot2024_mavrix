package frc.robot.subsystems.Swerve;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.sensors.AbsoluteSensorRange;
import com.ctre.phoenix.sensors.SensorInitializationStrategy;
import com.ctre.phoenix.sensors.SensorTimeBase;
import com.ctre.phoenix.sensors.WPI_CANCoder;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.SwerveModulePosition;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.PowerDistribution;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.util.CTREModuleState;
import frc.util.ConversionUtils;
import frc.util.LogManager;

public class SwerveModule extends SubsystemBase {
  //telemetry
  private final ShuffleboardTab swerveTab;

  //module location
  private final ModuleType moduleType;

  //offset of the module
  private final Rotation2d angleOffset;

  //Module Components
  private final WPI_TalonFX angleMotor;
  private final WPI_TalonFX driveMotor;
  private final WPI_CANCoder CanCoder;

  //swerve Module Desierd states
  private SwerveModuleState desiredState;
  private boolean optimizeStates = true;

  private boolean stateDeadband;
  // private PowerDistribution pdh;

  //Module feedForward
  SimpleMotorFeedforward feedforward = new SimpleMotorFeedforward(Constants.Swerve.DRIVE_KS, Constants.Swerve.DRIVE_KV, Constants.Swerve.DRIVE_KA);

  public SwerveModule(ModuleConstants moduleConstants , ShuffleboardTab shuffleboardTab) {
    //getting all parameters 
      this.swerveTab = shuffleboardTab;
      moduleType = moduleConstants.getType();
      // this.pdh = new PowerDistribution(0,edu.wpi.first.wpilibj.PowerDistribution.ModuleType.kRev);

      //configurating wheel offset
      angleOffset = new Rotation2d(moduleConstants.getSteerOffset());
      
      stateDeadband = true;

      //gets CanCoder and config it
      CanCoder = new WPI_CANCoder(moduleConstants.getEncoderPort());
      configCANcoder();

      angleMotor = new WPI_TalonFX(moduleConstants.getSteerPort());
      driveMotor = new WPI_TalonFX(moduleConstants.getDrivePort());

      configAngleMotor();
      configDriveMotor();

      desiredState = new SwerveModuleState(0, new Rotation2d());

      //setup shuffle board
      setupShuffleboard();

    }
    //sets Module Speed 
    private void setSpeed(SwerveModuleState desiredState, boolean isOpenLoop) {
      if (isOpenLoop) {
          double percentOutput = desiredState.speedMetersPerSecond / Constants.Swerve.kMaxSpeed;
          driveMotor.set(ControlMode.PercentOutput, percentOutput);
      } else {
          double velocity = ConversionUtils.MPSToFalcon(desiredState.speedMetersPerSecond, Constants.Swerve.kWheelCircumference,
                                                        Constants.Swerve.kDriveGearRatio);
          driveMotor.set(ControlMode.Velocity, velocity, DemandType.ArbitraryFeedForward,
                         feedforward.calculate(desiredState.speedMetersPerSecond));
      }
      if (Constants.logging) {
          double motorSpeed = ConversionUtils.falconToMPS(driveMotor.getSelectedSensorVelocity(), Constants.Swerve.kWheelCircumference,
                                Constants.Swerve.kDriveGearRatio);
          LogManager.addDouble("Swerve/Modules/DriveSpeed/" + moduleType.name(),
                               motorSpeed
                              );
          LogManager.addDouble("Swerve/Modules/DriveSpeedError/" + moduleType.name(),
                               motorSpeed - desiredState.speedMetersPerSecond
                              );
          LogManager.addDouble("Swerve/Modules/DriveVoltage/" + moduleType.name(),
                               driveMotor.getMotorOutputVoltage()
                              );
          LogManager.addDouble("Swerve/Modules/DriveCurrent/" + moduleType.name(),
                               driveMotor.getStatorCurrent()
                              );
      }
  }
  //stops all activity
  public void stop() {
    driveMotor.set(0);
    angleMotor.set(0);
}

    //set Module angle
    private void setAngle(SwerveModuleState desiredState) {
      // Prevent rotating module if desired speed < 1%. Prevents Jittering.
      if (stateDeadband && (Math.abs(desiredState.speedMetersPerSecond) <= (Constants.Swerve.kMaxSpeed * 0.01))) {
          stop();
          return;
      }
      angleMotor.set(ControlMode.Position, ConversionUtils.degreesToFalcon(desiredState.angle.getDegrees(), Constants.Swerve.kAngleGearRatio));
      if (Constants.logging) {
          double position = ConversionUtils.falconToDegrees(angleMotor.getSelectedSensorPosition(),
                                                            Constants.Swerve.kAngleGearRatio);
          LogManager.addDouble("Swerve/Modules/SteerPosition/" + this.moduleType.name(),
                               position
                              );
          LogManager.addDouble("Swerve/Modules/SteerPositionError/" + this.moduleType.name(),
                               position - desiredState.angle.getDegrees()
                              );
          LogManager.addDouble("Swerve/Modules/SteerVelocity/" + this.moduleType.name(),
                               ConversionUtils.falconToDegrees(angleMotor.getSelectedSensorVelocity(),
                                                               Constants.Swerve.kAngleGearRatio)
                              );
          LogManager.addDouble("Swerve/Modules/SteerVoltage/" + moduleType.name(),
                               angleMotor.getMotorOutputVoltage()
                              );
          LogManager.addDouble("Swerve/Modules/SteerCurrent/" + moduleType.name(),
                               angleMotor.getStatorCurrent()
                              );
      }
      }
  

    public void setDriveInverted(boolean state){
      driveMotor.setInverted(state);
    }

    public void setDesierdState(SwerveModuleState desierdState , boolean isOpenLoop){
      
      desierdState = 
            optimizeStates ? CTREModuleState.optimize(desierdState, getState().angle): 
                desiredState;  

      setAngle(desierdState);
      setSpeed(desierdState, isOpenLoop);
    }

    //reset all modules to absoloute zero
    public void resetToAbsolute() {
      double absolutePosition = ConversionUtils.degreesToFalcon(getCanCoder().getDegrees() - angleOffset.getDegrees(),
                                                                Constants.Swerve.kAngleGearRatio);
      angleMotor.setSelectedSensorPosition(absolutePosition);
  }

    public void enableStateDeadband(boolean enabled) {
      stateDeadband = enabled;
      LogManager.addBoolean("Swerve/Modules/StateDeadband/" + moduleType.name(), enabled);
  }

  public void setOptimize(boolean enable) {
      optimizeStates = enable;
      LogManager.addBoolean("Swerve/Modules/Optimized/" + moduleType.name(), enable);
  }

  public byte getModuleIndex() {
      return moduleType.id;
  }
    //get module angle 
    public Rotation2d getAngle() {
      return Rotation2d.fromDegrees(
              ConversionUtils.falconToDegrees(angleMotor.getSelectedSensorPosition(), Constants.Swerve.kAngleGearRatio));
  }
    //get encoder relative position
    public Rotation2d getCanCoder(){
      return Rotation2d.fromDegrees(CanCoder.getAbsolutePosition());
    }

    //get Module position
    public SwerveModulePosition getPosition() {
      return new SwerveModulePosition(
              ConversionUtils.falconToMeters(driveMotor.getSelectedSensorPosition(), Constants.Swerve.kWheelCircumference,
                                            Constants.Swerve.kDriveGearRatio),
              getAngle());
  }

  public SwerveModuleState getState() {
    return new SwerveModuleState(
            ConversionUtils.falconToMPS(driveMotor.getSelectedSensorVelocity(), Constants.Swerve.kWheelCircumference,
                                        Constants.Swerve.kDriveGearRatio),
            getAngle());
  }

  public SwerveModuleState getDesierdState(){
    return desiredState;
  }

  public SwerveModuleState getDesiredState() {
    return desiredState;
}

  public double getDesiredVelocity() {
      return getDesiredState().speedMetersPerSecond;
  }

  public Rotation2d getDesiredAngle() {
      return getDesiredState().angle;
  }

  public double getDriveVelocityError() {
      return getDesiredState().speedMetersPerSecond - getState().speedMetersPerSecond;
  }

  public double getDriveFeedForwardKV() {
      return Constants.Swerve.DRIVE_KV;
  }

  public double getDriveFeedForwardKS() {
      return Constants.Swerve.DRIVE_KS;
  }
  
  public double getSteerFeedForwardKV() {
    return 0;
}

public double getSteerFeedForwardKS() {
    return 0;
}


    //setup dashboard telemetry
    private void setupShuffleboard() {
      if (Constants.Telemetry) {
          swerveTab.addDouble(this.moduleType.name() + " CANcoder Angle (deg)", getCanCoder()::getDegrees);
          swerveTab.addDouble(this.moduleType.name() + " FX Angle (deg)", getPosition().angle::getDegrees);
          swerveTab.addDouble(this.moduleType.name() + " Velocity (m/s)", () -> getState().speedMetersPerSecond);
          swerveTab.addDouble(this.moduleType.name() + " Desired Velocity (m/s)", () -> getDesierdState().speedMetersPerSecond);
          swerveTab.addDouble(this.moduleType.name() + " Desired Angle (deg)", () -> getDesierdState().angle.getDegrees());
          swerveTab.addBoolean(this.moduleType.name() + " Jitter prevention enabled", () -> stateDeadband);
          swerveTab.addDouble(this.moduleType.name() + " Drive Current (A)", driveMotor::getSupplyCurrent);
          swerveTab.addDouble(this.moduleType.name() + " Angle Current (A)", angleMotor::getSupplyCurrent);
      }
  }

    //drive motor config
    private void configDriveMotor() {
      driveMotor.configFactoryDefault();
      driveMotor.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(
              Constants.Swerve.kDriveEnableCurrentLimit,
              Constants.Swerve.kDriveContinuousCurrentLimit,
              Constants.Swerve.kDrivePeakCurrentLimit,
              Constants.Swerve.kDrivePeakCurrentDuration
      ));
      driveMotor.config_kP(0, Constants.Swerve.kDriveP);
      driveMotor.config_kI(0, Constants.Swerve.kDriveI);
      driveMotor.config_kD(0, Constants.Swerve.kDriveD);
      driveMotor.config_kF(0, Constants.Swerve.kDriveF);
      driveMotor.configOpenloopRamp(Constants.Swerve.kOpenLoopRamp);
      driveMotor.configClosedloopRamp(Constants.Swerve.kClosedLoopRamp);
      driveMotor.setInverted(Constants.Swerve.kDriveMotorInvert);
      driveMotor.setNeutralMode(Constants.Swerve.kDriveNeutralMode);
      driveMotor.configVoltageCompSaturation(Constants.RobotVoltage);
      driveMotor.enableVoltageCompensation(true);
      driveMotor.setSelectedSensorPosition(0);
  }
    //angle motor config settings
    private void configAngleMotor() {
      angleMotor.configFactoryDefault();
      angleMotor.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(
              Constants.Swerve.kAngleEnableCurrentLimit,
              Constants.Swerve.kAngleContinuousCurrentLimit,
              Constants.Swerve.kAnglePeakCurrentLimit,
              Constants.Swerve.kAnglePeakCurrentDuration
      ));
      angleMotor.config_kP(0, Constants.Swerve.kModuleConstants.angleKP);
      angleMotor.config_kI(0, Constants.Swerve.kModuleConstants.angleKI);
      angleMotor.config_kD(0, Constants.Swerve.kModuleConstants.angleKD);
      angleMotor.config_kF(0, Constants.Swerve.kModuleConstants.angleKF);
      angleMotor.setInverted(Constants.Swerve.kAngleMotorInvert);
      angleMotor.setNeutralMode(Constants.Swerve.kAngleNeutralMode);
      angleMotor.configVoltageCompSaturation(Constants.RobotVoltage);
      angleMotor.enableVoltageCompensation(true);
      resetToAbsolute();
  }
    //CanCoder config function
    private void configCANcoder() {
      CanCoder.configFactoryDefault();
      CanCoder.configAbsoluteSensorRange(AbsoluteSensorRange.Unsigned_0_to_360);
      CanCoder.configSensorDirection(Constants.Swerve.kModuleConstants.canCoderInvert);
      CanCoder.configSensorInitializationStrategy(SensorInitializationStrategy.BootToAbsolutePosition);
      CanCoder.configFeedbackCoefficient(0.087890625, "deg", SensorTimeBase.PerSecond);
  }
  
  public void setDriveFeedForwardValues(double kS, double kV) {
  }

  @Override
  public void periodic() {

    // This method will be called once per scheduler run
  }
}
