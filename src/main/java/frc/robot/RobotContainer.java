package frc.robot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.pathplanner.lib.auto.AutoBuilder;
import com.pathplanner.lib.auto.NamedCommands;
import com.pathplanner.lib.commands.FollowPathHolonomic;
import com.pathplanner.lib.commands.FollowPathWithEvents;
import com.pathplanner.lib.commands.PathPlannerAuto;
import com.pathplanner.lib.path.PathConstraints;
import com.pathplanner.lib.path.PathPlannerPath;
import com.pathplanner.lib.util.HolonomicPathFollowerConfig;
import com.pathplanner.lib.util.PIDConstants;
import com.pathplanner.lib.util.ReplanningConfig;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.DriverStation.Alliance;
import edu.wpi.first.wpilibj.livewindow.LiveWindow;
import edu.wpi.first.wpilibj.shuffleboard.EventImportance;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.PrintCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.Systems.Frizzle;
import frc.robot.Systems.SelfStraight;
import frc.robot.Systems.StockerOdometry;
import frc.robot.Systems.StockerOnRing;
import frc.robot.commands.DefaultShooter;
import frc.robot.commands.SwerveDefaultCommand;
import frc.robot.subsystems.Swerve.ShooterDefault;
import frc.robot.subsystems.Swerve.SwerveSubsystem;
import frc.util.PS5ControllerDriverConfig;
import frc.util.Vision;

public class RobotContainer {

  
    // Shuffleboard auto chooser
    // private final SendableChooser<Command> autoCommand = new SendableChooser<>();

    //shuffleboard tabs
    // The main tab is not currently used. Delete the SuppressWarning if it is used.
    @SuppressWarnings("unused")
    private final ShuffleboardTab mainTab = Shuffleboard.getTab("Main");
    private final ShuffleboardTab drivetrainTab = Shuffleboard.getTab("Drive");
    private final ShuffleboardTab swerveModulesTab = Shuffleboard.getTab("Swerve Modules");
    private final ShuffleboardTab autoTab = Shuffleboard.getTab("Auto");
    private final ShuffleboardTab controllerTab = Shuffleboard.getTab("Controller");
    private final ShuffleboardTab visionTab = Shuffleboard.getTab("Vision");

   private final Vision cameraVisiom  = new Vision("Thor-Camera" , Constants.VisionConstants.CAMERA_TO_ROBOT);
   private final Vision limelightVision  = new Vision("MavriXCam" , Constants.VisionConstants.APRILTAG_CAMERA_TO_ROBOT);;

 
    // The robot's subsystems are defined here...
    private SwerveSubsystem drive = new SwerveSubsystem(drivetrainTab, swerveModulesTab, cameraVisiom , limelightVision);

    // Controllers are defined here
   private final PS5ControllerDriverConfig driver;
   private final PS5ControllerDriverConfig operator;

  ShooterDefault shooterDefault;
  Joystick driverJoystick;

  Pose2d[] pose2dTargets = {new Pose2d(0,0,new Rotation2d(12)), new Pose2d(3,1,new Rotation2d(90))};
   public RobotContainer(ShooterDefault shooterDefault) {
      driver = new PS5ControllerDriverConfig(this.drive, controllerTab , Constants.Telemetry);
      operator = new PS5ControllerDriverConfig(this.drive, controllerTab, false);
      driver.configureControls();
      this.shooterDefault = shooterDefault;
      driverJoystick = new Joystick(0);

      shooterDefault.setDefaultCommand(new DefaultShooter(shooterDefault, driver));

      drive.setDefaultCommand(new SwerveDefaultCommand(this.drive, driver , controllerTab));
        
      // This is really annoying so it's disabled
      DriverStation.silenceJoystickConnectionWarning(true);
      LiveWindow.disableAllTelemetry(); // LiveWindow is causing periodic loop overruns
      LiveWindow.setEnabled(false);

      // autoTab.add("Auto Chooser", autoCommand);

        
      // if(DriverStation.getAlliance().get() == Alliance.Red){
            drive.setYaw(new Rotation2d(180));
        // }else{
        //     drive.setYaw(new Rotation2d(180));

        //  }


        if (Constants.Telemetry) loadCommandSchedulerShuffleboard();

        driver.setupShuffleboard();

    configureBindings();
  }

  private void configureBindings() {
    driver.configureControls();

    new JoystickButton(driverJoystick, 6).onTrue(new StockerOdometry(drive, driverJoystick));
    new JoystickButton(driverJoystick, 3).onTrue(new Frizzle(drive, driverJoystick, pose2dTargets));
    // new JoystickButton(driverJoystick, 7).onTrue(new StockerOnRing(drive, driverJoystick));
    new JoystickButton(driverJoystick, 2).onTrue(new SelfStraight(drive, driverJoystick));
    
}


  public Command getAutonomousCommand() {
    return new SequentialCommandGroup(
        new InstantCommand(() -> this.drive.resetYaw()),
        new InstantCommand(() -> this.drive.resetOdometry(PathPlannerPath.fromPathFile("Auto2path1").getPreviewStartingHolonomicPose())), 
        new InstantCommand(() -> this.drive.resetModulesToAbsolute()), 
        drive.getAutoBuilder().buildAuto("Auto3")
    );
    
  }

  public void loadCommandSchedulerShuffleboard() {
      // Set the scheduler to log Shuffleboard events for command initialize, interrupt, finish
      CommandScheduler.getInstance().onCommandInitialize(command -> Shuffleboard.addEventMarker("Command initialized", command.getName(), EventImportance.kNormal));
      CommandScheduler.getInstance().onCommandInterrupt(command -> Shuffleboard.addEventMarker("Command interrupted", command.getName(), EventImportance.kNormal));
      CommandScheduler.getInstance().onCommandFinish(command -> Shuffleboard.addEventMarker("Command finished", command.getName(), EventImportance.kNormal));
  
    }


}
