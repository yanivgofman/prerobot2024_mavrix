// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.Systems;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.commands.SwerveDefaultCommand;
import frc.robot.subsystems.Swerve.SwerveSubsystem;
import frc.util.PS5ControllerDriverConfig;

public class Frizzle extends Command {
  SwerveSubsystem swerve;
  Joystick joystick;
  Pose2d[] poses;
  Pose2d targetPose;
  double minDist = 0;
  public Frizzle(SwerveSubsystem swerve, Joystick joystick, Pose2d[] poses) {
    this.joystick = joystick;
    this.swerve = swerve;
    this.poses = poses;
    targetPose = new Pose2d();
    addRequirements(swerve);
  }

  public double distance(Pose2d current, Pose2d target) {
    return Math.sqrt(Math.pow(current.getX() - target.getX(), 2) + Math.pow(current.getY() - target.getY(), 2));
  }

  @Override
  public void initialize() {
    minDist = distance(swerve.getPose(), poses[0]);
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    
    for(int i =0; i < poses.length;i++) {
      if(distance(swerve.getPose(), poses[i]) < minDist) {
        minDist = distance(swerve.getPose(), poses[i]);
        targetPose = poses[i];
      }
    }

    swerve.driveToPose(
      targetPose,
      true
    );
  }
  @Override
  public void end(boolean interrupted) {}

  @Override
  public boolean isFinished() {
    if(Math.abs(joystick.getRawAxis(1))>0.05 || Math.abs(joystick.getRawAxis(0)) > 0.05 || Math.abs(joystick.getRawAxis(2)) > 0.05) {
      return true;
    }
    return false;
  }
}
