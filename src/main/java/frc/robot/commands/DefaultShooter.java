// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import org.opencv.core.Mat;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.sysid.SysIdRoutine.Mechanism;
import frc.robot.subsystems.Swerve.ShooterDefault;
import frc.util.PS5ControllerDriverConfig;

public class DefaultShooter extends Command {
  ShooterDefault shooterDefault;
  PS5ControllerDriverConfig Driver;

  public DefaultShooter(ShooterDefault shooterDefault , PS5ControllerDriverConfig Driver) {
    this.shooterDefault = shooterDefault;
    this.Driver = Driver;
    addRequirements(shooterDefault);
  }
  

  @Override
  public void initialize() {}

  @Override
  public void execute() {
      if(Driver.Collect()){        
        shooterDefault.setIntake(0.4);
        shooterDefault.setCollector(0.6);
      }else if(Driver.Shoot()){
        // shooterDefault.setIntake(-1);
        shooterDefault.setShooterVelocity(-18500.0);
        if(shooterDefault.reachedVelocity())
            shooterDefault.setCollector(-1);
      }else if(Driver.ShootingPose()){
          // shooterDefault.setArmPosition(500);
          shooterDefault.setArmPosition(190000);
      }else if(Driver.CollectPose()){
          // shooterDefault.setArmPosition(500);
          shooterDefault.setArmPosition(0);

      }else if(Driver.ShootingZone()){
          // shooterDefault.setArmPosition(500);
          shooterDefault.setArmPosition(170000);

      }else if(Math.abs(Driver.getArm())>0.1){
        shooterDefault.setArm(-Driver.getArm());
      }else{
        shooterDefault.setIntake(0.0);
        shooterDefault.setCollector(0.0);
        shooterDefault.setArm(0);
      }
      // System.out.println(shooterDefault.getArmPosition());
  }

  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
