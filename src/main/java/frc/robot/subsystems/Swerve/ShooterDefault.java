package frc.robot.subsystems.Swerve;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ShooterDefault extends SubsystemBase {
  TalonFX right;
  TalonFX left;
  TalonFX arm;
  TalonFX shooter;

  public ShooterDefault() {
    right = new TalonFX(51);
    left = new TalonFX(10);
    arm = new TalonFX(53);
    shooter = new TalonFX(56);
    right.setInverted(true);
    arm.setNeutralMode(NeutralMode.Brake);
  

    shooter.config_kP(0, 0.05);
    shooter.config_kI(0, 0.06);
    shooter.config_kD(0, 0.00);
    shooter.configAllowableClosedloopError(0,0 , 20);

    arm.config_kP(0 , 0.1);
    arm.config_kI(0, 0);
    arm.config_kD(0,0);
    arm.configAllowableClosedloopError(0, 1000);

  }
  public void setIntake(){
    right.set(ControlMode.PercentOutput, 0.7);
   
  }
  public void setAll(double value){
    right.set(ControlMode.PercentOutput, value);
    left.set(ControlMode.PercentOutput, value);
    shooter.set(ControlMode.PercentOutput, value);

  }
  public void setAllBreak(){
    right.setNeutralMode(NeutralMode.Brake);
    left.setNeutralMode(NeutralMode.Brake);
    shooter.setNeutralMode(NeutralMode.Brake);
    
  }
  public void setAllCoast(){
    right.setNeutralMode(NeutralMode.Coast);
    left.setNeutralMode(NeutralMode.Coast);
    shooter.setNeutralMode(NeutralMode.Coast);

  }
  public void setShooterVelocity(double velocity){
      shooter.set(ControlMode.Velocity, velocity);

  }
  public boolean reachedVelocity(){
    return shooter.getSelectedSensorVelocity() < -18000 &&  shooter.getSelectedSensorVelocity() > -20000;
  }

  public double getVelocity(){
    return shooter.getSelectedSensorVelocity();
  }
  public void setCollector(double value){
    right.set(ControlMode.PercentOutput, value);
    left.set(ControlMode.PercentOutput, value);
  }
  public void setArm(double value){
    arm.set(ControlMode.PercentOutput,value);

  }
  public void setIntake(double value){
    shooter.set(ControlMode.PercentOutput, value);
  }
  //max position - 200000
  public double getArmPosition(){
    return arm.getSelectedSensorPosition();
  }

  public void setArmPosition(double position){
    arm.set(ControlMode.Position, position);
  }

  @Override
  public void periodic() {
    // This method will be called once per scheduler run
  }
}
